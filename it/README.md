# Raspberry Pi Desktop Challenge
## #pidesktopchallenge

---

In questa repo, che userò come un semplice blog/diario,
scrivendo tutto in Markdown,
documenterò il mio andamento della Raspberry Pi Desktop Challenge,
sfida che, come il nome suggerisce, consiste nell'usare il
Raspberry come PC desktop principale per un po' di tempo.  
Nel mio caso, per una settimana userò il Pi 3.

### Background

Negli anni, la community del rasperino ha dato una possibilità alla
Raspberry Pi Desktop Challenge, per diletto e per vedere fino a che punto
si potesse spingere il piccolo single board computer.

È stata abbastanza popolarizzata con l'uscita del Pi 4 nel 2019,
venduto con un SoC molto più potente e disponibile con 4GB di RAM,
in gran parte per via del fatto che la Raspberry Foundation
ha per la prima volta provato a vendere il prodotto
non solo come uno strumento di sperimentazione e apprendimento per le nicchie,
ma anche come un valido PC desktop con costi bassi e profilo ridottissimo.  
Con queste pretese, molte persone si sono incuriosite, e hanno deciso di
tentare la sfida, per provare con mano se ciò che si dicesse fosse vero.
Qualcuno ha avuto successo, qualcun altro dice
di non aver passato proprio un bel quarto d'ora (o meglio, di mese).

Si può quasi dire che questa sfida sia ufficiale,
in quanto la 59esima edizione del MagPi, pubblicata nel 2017,
presenta proprio un racconto della cosa, come si può leggere qui:
[magpi.raspberrypi.com/articles/raspberry-pi-desktop-pc](https://magpi.raspberrypi.com/articles/raspberry-pi-desktop-pc).  
Bisogna tenere a mente che, ai tempi, il Pi più all'avanguardia era il 3,
con un SoC che per gli standard odierni è obsoleto, e solo 1GB di RAM.

### In pratica

Sarebbe bello mettere tutto ciò in pratica con un Raspberry Pi 4,
o anche il più recente Pi 400, particolare per il suo aspetto retrò.  
Gran peccato però, che io ho solo un Pi 3 Model B, e quindi
dovrò affrontare quest'odissea con ferri vecchi.

Come può anche soltanto venirmi in mente di sottopormi a tutto questo,
se non ho nemmeno l'ultimo modello, vi chiederete. Beh.. sono un po' stanca
di tutte le persone che affermano come anche gli ultimi Pi NON siano capaci
di rimpiazzare un PC desktop, che non si può usare il computer lampone
per roba seria, e altre frasi che onestamente vedo come inaccurate.

Capisco però perché non riesco a mettere a tacere queste opinioni
solo citando la mia esperienza personale che è, almeno fino a questo momento,
onestamente limitata a singoli lavori in situazioni sempre diverse.
Esperienze del genere non possono ovviamente catturare la natura dell'usare
una singola macchina come primaria
per un buon periodo di tempo, come una settimana.

L'unico modo per provare che il rasperino è adatto come PC desktop,
è di usarlo veramente come PC desktop.  
E quindi, se riesco a provare ciò usando un Pi 3, allora
non si può proprio più controbattere su come almeno i modelli nuovi
siano più che adeguati come computer casalinghi principali.
