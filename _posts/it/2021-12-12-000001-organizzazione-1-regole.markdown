---
layout: post
title:  "Organizzazione (1/3): Regole"
date:   2021-12-12
categories: it
---

#### [ Torna alla [Home]({{ site.baseurl }}/it/) ]

---

# Organizzazione della sfida (1/3) - Le regole

Per una sfida chiara e ben strutturata, è bene scrivere alcune cose.
In particolare:
- Le regole da mettere in chiaro e rispettare
- Gli obiettivi
- Le strategie

In questa pagina approfondiremo tutte le regole del caso.

### Regole di base

Le regole di questa sfida sarebbero quelle di usare soltanto il Raspberry
come PC desktop principale per un po' di tempo.
Si può vedere ciò in modo più lineare:
- È OK l'usare altri dispositivi che di solito già si usava in modo abituale,
  che non sono il PC desktop che si sta sostituendo
- Non è OK iniziare ad usare dispositivi che fino a quel momento si è tenuti
  in disuso perché il computer desktop abituale riusciva a svolgere le
  funzioni desiderate adeguatamente

Nel mio caso quindi, potrò continuare ad usare smartphone, console da gioco,
o qualsiasi altro oggetto che userei per funzioni che di solito non ho bisogno
di fare dal PC.  
È un po' una zona grigia quella di utilizzare di più i dispositivi
che già si usava prima abitualmente, solo per sopperire
a delle mancanze che possono sorgere fuori usando il Pi come PC.
Quindi, cercherò di mantenere i miei workflow con gli altri dispositivi
uguali al normale, ma renderò chiaro ogni caso specifico
se dovessero esserci variazioni nei miei modi di utilizzo.

### Regole avanzate

Oltre alle regole di base che seguirò, ho considerato anche altri dettagli.  
Considerando che lo scopo di questa sfida è quello di
dimostrare che il rasperino è adatto ad uso desktop,
tutto ciò che sarebbe tranquillamente possibile con solo il rasperino è giusto.

Nel mio PC desktop normale ho un HDD da 1TB.
Gli HDD si possono collegare senza problemi al Pi usando adattatori USB e,
nello specifico, per il mio disco ne servirebbe uno SATA USB
esternamente alimentato, dato che è da 3.5".  
Purtroppo, io questo adattatore non lo ho, e non ho modo di comprarlo solo
per fare questa sfida. Ma dato che è certamente possibile comprarne uno
ed usarlo sul Raspberry, come su qualsiasi altro computer
che supporti dispositivi di archiviazione USB,
si può dare la dimostrazione di questo dettaglio come verificata positivamente,
e quindi posso risolvere il mio problema di archiviazione in altro modo.

Sul mio PC solito, collegato in WLAN,
installerò un server SSH che parte all'avvio.
Quando devo accedere al mio disco da 1TB dal raspino,
semplicemente accendo il PC e, dopo qualche minuto di attesa,
potrò accedere a tutto il file system del PC.

Solamente questo. Non installerò ovviamente nessun software di NAS complesso
sul PC, niente roba che organizza da sola i file, che li comprime,
o che fa altre operazioni che utilizzano risorse sul computer host.  
Sostanzialmente, è come se avessi il disco collegato via USB,
con tutte le varie operazioni di gestione dei file
che sforzano solo il Raspberry, e non il mio computer solito.  
Tralaltro, ci sarà anche un bottleneck pesante, perché se tutto va
secondo i tragici piani (ci arriviamo dopo),
l'adattatore WLAN che potrò riservare al PC fisso non sarà affatto performante.

### Conclusione

Le regole qui sopra approfondite sono secondo me semplici ma oneste...
Nulla di cui gioire troppo, però.
Le istruzioni saranno anche semplici, si,
ma la difficoltà viene poi nel mettere tutto in pratica.

Alla pagina successiva inizierò ad illustrare gli obiettivi di questa sfida,
sia gli scopi di essa, sia i modi di portarla avanti, facendo cosa e come.

---

#### Navigazione dei post
Precedente: [Introduzione alla sfida]({{ site.baseurl }}/it/2021/12/11/000000-introduzione.html)  
~  
Successivo: [Organizzazione (2/3): Obiettivi]({{ site.baseurl }}/it/2021/12/12/000002-organizzazione-2-obiettivi.html)
