---
layout: post
title:  "Organizzazione (2/3): Obiettivi"
date:   2021-12-12
categories: it
---

#### [ Torna alla [Home]({{ site.baseurl }}/it/) ]

---

# Organizzazione della sfida (2/3) - Gli obiettivi

Per una sfida chiara e ben strutturata, è bene scrivere alcune cose.
In particolare:
- Le regole
- Gli obiettivi della challenge
- Le strategie

In quest'altra pagina, vedremo quali sono gli obiettivi della sfida.
Andando più nello specifico, si può parlare innanzitutto degli obiettivi
con valore di sperimentazione e crescita personale della sfida,
ma anche a livello pratico: che scopi si deve raggiungere?

### Cose da fare

Molte di queste cose le faccio abitualmente sul mio PC desktop,
altre potrei averne bisogno o comunque sono cose che usualmente si fanno.

Ordino bene o male da quella che si prospetta meglio alla peggio:
- Buona personalizzazione visiva nonostante le risorse limitate
- Scrittura di documenti
- Messaggistica instantanea e non
- Programmazione e sviluppo software
- Social network, streaming multimediale, eccetera...
- Navigazione web confortevole
- Gestione avanzata di file (archiviare, caricare e scaricare, ecc...)
- Editing fotografico
- Videogiocare
- Registrare, trasmettere, ed editare video
- Compressione di file, trascoding multimediale, e così via...
- Testing di software e strumenti correlati
  (VM, containers, software da compilare, ecc..)

Per ogni cosa, gli sbocchi possibili sono molteplici.
Possono esserci tanti modi di fare, o diverse piccole cose correlate.  
Durante la settimana della sfida, ovviamente, ogni obiettivo prefissato
verrà affrontato e approfondito.  
Se mi gira, posso provare a fare altre cose che ora non mi vengono in mente,
più o meno particolari, si vedrà sul campo.

### Obiettivi (si spera) acquisiti

Come già detto nell'[introduzione]({{ site.baseurl }}/it/2021/12/11/00-introduzione.html),
il mio motivo per questa sfida è quello di dimostrare che, se non si hanno
troppe pretese, il Raspberry è un valido PC casalingo con i suoi vantaggi:
la compattezza, l'efficienza e il bassissimo consumo energetico,
la silenziosità, l'eleganza, e la flessibilità.

Passare dal mio Ryzen 3 3200G con 16 GB di RAM
ad un ARM Cortex-A53 con 1 GB di RAM da un giorno all'altro
sicuramente non sarà semplice, però.  
Per poter sopravvivere, quindi, sicuramente dovrò cambiare un po'
le mie abitudini digitali. Su questo ne discuterò poi nella prossima pagina,
ma possiamo dare per certo che l'esperienza sarà preziosa.  
Infatti, sono abbastanza sicura che con una sfida del genere posso imparare a:
- Mantenere delle abitudini digitali più minimali
- Imparare a conoscere bene il mio sistema Linux e le app disponibili,
  e scegliere configurazioni e strumenti sempre efficienti
- Imparare a fare tanto con poco, una capacità che può tornare utile
  in generale in situazioni particolari
- Apprezzare meglio l'evoluzione enorme della tecnologia negli ultimi tempi,
  specialmente di quella embedded, che ha reso possibili prodotti come il Pi

Ad ogni modo, la cosa migliore è iniziare il tutto con una visione ottimista:
il rasperino è pefetto, se negli anni '80 bastava un 8086 e 640 KB di RAM...

### Conclusione

Con questi obiettivi, abbiamo dato anche una baseline sulle mie aspettative e,
spero di non aver dimenticato qualcosa a riguardo, le aspettative della
fascia più generalista tra quelle che utilizza i computer.

Non resta altro che gli ultimi preparativi, e la messa in piedi di
alcune strategie atte a muoversi bene nella palude del computing da discount.

---

#### Navigazione dei post
Precedente: [Organizzazione (1/3): Regole]({{ site.baseurl }}/it/2021/12/12/000001-organizzazione-1-regole.html)  
~  
Successivo: [Organizzazione (3/3): Strategie]({{ site.baseurl }}/it/2021/12/13/000003-organizzazione-3-strategie.html)
