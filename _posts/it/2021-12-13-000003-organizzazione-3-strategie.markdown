---
layout: post
title:  "Organizzazione (3/3): Strategie"
date:   2021-12-13
categories: it
---

#### [ Torna alla [Home]({{ site.baseurl }}/it/) ]

---

# Organizzazione della sfida (3/3) - Le strategie

Per assicurarci che la sfida sia chiara e ben strutturata,
è bene mettere per iscritto alcune cose.
In particolare:
- Le regole
- Gli obiettivi
- Le strategie da adottare per poter avere una possibilità

In quest'ultima pagina, andrò ad illustrare quelle che sono appunto
le mie strategie, mirate ad avere una buona serie di possibilità
di arrivare alla fine della challenge ancora tutta intera.

### La distribuzione

Come distro Linux da installare sul Raspberry, ho scelto la classica
Raspberry Pi OS (32-bit), basata su Debian Bullseye da circa un mese.

I miei motivi per la scelta sono diversi:
- Il sistema è abbastanza leggero dal primo momento,
  rispetto a molte altre distro disponibili per il raspino;
  lo dimostrano sia le statistiche che la mia esperienza passata
- È non solo la distro con il miglior supporto generale, quindi
  molti software saranno più ottimizzati per essa su Raspberry,
  ma, in particolare la versione 32-bit,
  è a quanto so l'unica con supporto hardware veramente completo,
  specialmente per quanto riguarda la GPU.

### Configurazione hardware (per quel poco possibile)

Per quanto non si possa parlare molto riguardo la configurazione hardware
se si tratta di un rasperino, voglio parlare del possibile.

Riguardo la microSD, ho scelto una Samsung EVO Plus, 64 GB, UHS-I U3.
È la più capiente e veloce in casa che posso usare al momento,
quindi tanto vale usare questa.

Per il collegamento alla rete, non userò la scheda WiFi interna del mio Pi,
non solo perché è soltanto 2.4GHz, ma anche perché, non avendo il computerino
una vera antenna WiFi, le prestazioni saranno alquanto scadenti
considerando il posizionamento nella mia stanza.  
Userò il mio bridge Ethernet TP-Link AC750,
lo stesso che ho fino ad ora ho usato per il mio solito PC desktop,
per collegarmi ad Internet.

A proposito di ciò, il PC solito una scheda WiFi non la ha, quindi,
come accennato nel post precedente, dovrò tenerlo collegato in WLAN
al fine di usarlo come semplice disco di rete con...
un piccolo bridge WiFi Ethernet di una marca generica,
abbastanza scrauso, solo 2.4GHz,
e che neanche so se veramente ce la farà anche soltanto
a mantenere un collegamento con il router che sta nell'altra stanza.
In alternativa, o usare una chiavettina USB WiFi ancora più scrausa.

Vogliamo parlare dell'overclock, di CPU, GPU, e RAM?
Non ho ben capito se sul mio modello è fattibile, ma ci proverò.
Ho un alimentatore da 3A, come consigliato dalla Raspberry Foundation,
quindi non ci dovrebbero essere problemi di corrente insufficiente,
e quindi nessun underclock forzato.

### Il software

Per quanto per raggiungere alcuni degli obiettivi discussi
nella pagina di prima, il software incluso in Raspberry Pi OS è forse
l'unica alternativa utile.

Come programmi nativi, è meglio preferire quello che è incluso nelle
repo predefinite, evitando di compilare da sorgente quando possibile,
e soprattutto di evitare soluzioni come Snap, problematiche già su
hardware alto, figurarsi sul raspante.  
Sarà meglio preferire comunque solo app native:
quelle web, se molto grosse, potrebbero darci alcuni problemi,
considerando che le moderne girano solo su Chromium e Firefox,
entrambi browser web che velocemente mangiano
le limitate risorse del Pi, RAM in special modo.

Un'altra cosa da considerare è che forse converrà spendere un po' di tempo
a trovare app particolarmente leggere e minimali per fare ciò che si deve,
con l'aiuto di semplici motori di ricerca,
o prendendo spunto dalla lista dei pacchetti di distro rinomate
per il loro puntare alla leggerezza e alla compatibilità pratica
con computer molto vecchi, come ad esempio Puppy Linux.

### Alcuni aggiustamenti

Raspberry Pi OS non è messo proprio al meglio del meglio di default.

Innanzitutto, nell'ultima versione, sembra esserci un bug che
impedisce a NTP di funzionare. Ciò vuol dire che l'orario di sistema
non si aggiornerà in automatico dalla rete, il che è un problema,
in quanto il raspino non ha modo di tenere l'orario dopo lo spegnimento.  
Bisognerà vedere di sistemare questo problema, o toccherà aggiornare l'orario
a mano ad ogni riaccensione del computer. Bisognerà comunque farlo sicuramente la prima volta, con il comando `sudo date -s'yyyy-mm-ddThh:mm'`.

Poi, nella IMG consigliata, ci sono numerosi pacchetti inutili preinstallati,
che andrò a togliere per alleggerire il sistema.  
Toglierò probabilmente anche il DE LXDE, lasciando solo il WM Openbox,
(se riesco a non spaccare tutto il sistema nel processo) perché,
quando hai 1GB di RAM, ogni singolo MB purtroppo conta.  
Non ho scelto di installare l'immagine Lite dell'OS,
per installare solo poi Xorg ed Openbox a mano,
perché in questo mese su Bullseye ho avuto una brutta esperienza a riguardo.

In secondo luogo, bisogna disattivare alcune funzioni integrate nel sistema.
La prima cosa che mi viene in mente è il logging costante su memoria
di archiviazione, fatto senza ritegno da journald e rsyslog.  
Queste operazioni non solo possono sprecare preziose risorse su
hardware scarso come il computer lampone ma, se scrivono in continuazione
dati sulla scheda SD, molto banalmente la consumano in poco: le schede SD
economiche sono tutte sensibilissime alle troppe scritture e possono
morire in relativamente poco tempo. E io non voglio rovinare le mie schede.

### Conclusione

Avendo questi particolari appena scritti bene in testa,
le probabilità di non soffrire durante la settimana della sfida
si alzano almeno un po'.

Ma adesso, che tutta la pianificazione è finita,
e io ho già preparato la mia SD da mettere nel Raspberry,
non mi resta soltanto che iniziare la sfida.  
Questa giornata (e ormai è sera), fino alla prossima settimana,
sarà l'ultima in cui userò il performante PC
che ormai da 1 anno e mezzo mi serve egregiamente.

---

#### Navigazione dei post
Precedente: [Organizzazione (1/3): Regole]({{ site.baseurl }}/it/2021/12/12/000001-organizzazione-1-regole.html)  
~  
Successivo: - [Il vero inizio]({{ site.baseurl }}/it/2021/12/15/000004-primi-2-giorni.html)
