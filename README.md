# Raspberry Pi Desktop Challenge
## #pidesktopchallenge

### Other Languages
- [Italiano](it/)

---

In this repo, that I will use as a really simple blog/diary,
writing all my stuff in Markdown,
I will document my attempt at the Raspberry Pi Desktop Challenge,
performed on a Pi 3 and during one week.

**Note:** I will be writing my daily posts only in Italian.  
Maybe I will translate or readapt everything to English
after completing the challenge, and that's the reason
for writing this basic README in English,
but I don't guarantee anything yet.

### Background

The Raspberry Pi Desktop Challenge is something that in many years
the Pi community has attempted, as a way to have fun and to see
how far the little single board computer could be taken.

It has been rather popularized with the release of the Pi 4 model in 2019,
shipped with a much more powerful SoC and available with 4GB of RAM,
as the Raspberry Foundation itself tried for the first time to market
the product as not only a learning and thinkering tool for the niche
community, but also as a valid cheap and low profile desktop PC.  
Seeing this pretentious claim, curiosity sparked in many people, who
took this challenge on themselves to try first-hand if the saying was true.
Some more or less succeeded, others say they didn't really have a fun time.

We could say this challenge is actually official, as issue 59 of The MagPi,
from 2017, features this exact format, as you can read here:
[magpi.raspberrypi.com/articles/raspberry-pi-desktop-pc](https://magpi.raspberrypi.com/articles/raspberry-pi-desktop-pc).  
Keep in mind that, at the time, the latest Pi was the 3,
with a SoC that is by today's standards obsolete, and only 1GB of RAM.

### In practice

It would be nice to put this into practice with a Raspberry Pi 4,
or even the more recent and retro-inspired Pi 400.  
Sad thing I only have a Pi 3 Model B, and that means I will have
to tackle all this with old tools.

Why would I even think of doing this, if I don't even have the latest model,
you ask? Well.. I am kind of tired of all the people saying
even the latest Pis are NOT capable of replacing a desktop PC,
that there's no way you could really use the fruity board for anything serious,
and other claims I honestly see as inaccurate.

I do, however, understand why I can't put these opinions to sleep by citing
my personal experience which is, honestly, to this day limited
to single tasks in different situations.
Experiences like this can't perfectly capture the nature of using
a single machine as your primary one for a good period of time, like a week.

The only way to prove that the Raspberry is adequate as a desktop PC,
is to actually use it as a desktop PC.  
And so, if I can prove this using a Pi 3, then there's absolutely
no space for doubt that the newer and more advanced models are
more than capable of being used as the main house computers.
